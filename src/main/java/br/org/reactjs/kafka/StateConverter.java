package br.org.reactjs.kafka;

import javax.enterprise.context.ApplicationScoped;

import org.eclipse.microprofile.reactive.messaging.Incoming;
import org.eclipse.microprofile.reactive.messaging.Outgoing;

import io.smallrye.reactive.messaging.annotations.Broadcast;

@ApplicationScoped
public class StateConverter {
	
	@Broadcast
	@Incoming("ViewStates")
	@Outgoing("view-states")
	public String inputState(final String state) {
		return state;
	}
	
}