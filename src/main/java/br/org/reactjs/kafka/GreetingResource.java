package br.org.reactjs.kafka;

import static javax.ws.rs.core.MediaType.SERVER_SENT_EVENTS;
import static javax.ws.rs.core.MediaType.TEXT_PLAIN;

import javax.inject.Inject;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;

import org.eclipse.microprofile.reactive.messaging.Channel;
import org.jboss.resteasy.annotations.SseElementType;
import org.reactivestreams.Publisher;

@Path("/state-manager")
public class GreetingResource {

	@Inject
	@Channel("view-states")
	Publisher<String> states;
	
    @GET
    @Path("/stream")
    @SseElementType(TEXT_PLAIN)
    @Produces(SERVER_SENT_EVENTS)
    public Publisher<String> stateStream() {
        return this.states;
    }
}