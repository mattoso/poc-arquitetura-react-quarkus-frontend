import React from 'react';

var app = {}

window.app = app;

app.viewMap = {};
app.viewFactory = {};
app.viewStateMap = {};

app.createView = viewId => {
  if (viewId === undefined) {
    return null;
  }
  return app.viewFactory[viewId](viewId);
}

class ViewComponent extends React.Component {
  constructor(props) {
    super(props);
    this.state = {}
  }
  componentWillUnmount() {
    let viewStateId = this.props.viewStateId;
    if (viewStateId) {
      delete app.viewMap[viewStateId];
    }
  }
}

app.ViewComponent = ViewComponent;

app.onAction = (viewId, actionId = 0) => {
   
};

app.onStart = () => {
  let eventSource = new EventSource("/state-manager/stream");
  eventSource.onmessage = state => {
    console.log(`state atualizado: ${state}`)
    _updateState(state);
  }
}

app.onStop = () => {

}

app.updateState = state => {
  _updateState(state);
};

const _updateState = state => {
  let viewElm = app.viewMap[state.viewId];
  if (viewElm) {
    let same = false;
    if (viewElm.state && state) {
      same = JSON.stringify(viewElm.state) === JSON.stringify(state);
    }
    if (!same) {
      viewElm.setState(state || {});
    }
  }
}

export default app;