import app from '../../fw/app.js';

class HomeView extends app.ViewComponent {
  static register() {
    app.viewFactory['6fa183002902'] = viewId => <HomeView key={viewId} viewStateId={viewId} />;
  }

  constructor(props) {
    super(props);
  }

  componentDidMount() {
    app.onAction(this.props.viewStateId, 0);
  }

  render() {
    return (
      <div style={{ backgroundColor: this.state.color }}></div>
    )
  }
}

export { HomeView };
