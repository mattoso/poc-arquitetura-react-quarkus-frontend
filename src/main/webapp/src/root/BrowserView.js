import app from '../fw/app.js';

class BrowserView extends app.ViewComponent {

  constructor(props) {
    super(props);

    this.state = {
      homeViewId:"6fa183002902"
    };
  }

  componentWillMount() {
    app.onStart();
  }
  
  componentWillUnmount() {
    super.componentWillUnmount();
  }

  render() {
    let homeView = this.state.homeViewId ? app.createView(this.state.homeViewId) : "Nenhuma view padrao";
    return (
      <div>
        {homeView}
      </div>
    )
  }
}

export { BrowserView };
