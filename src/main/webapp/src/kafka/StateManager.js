import app from '../fw/app.js';

let eventSource = new EventSource("/state-manager/stream");
eventSource.onmessage = state => {
  console.log(`state atualizado: ${state.data}`)
  app.updateState(state.data);
}