const { Kafka } = require("kafkajs");

const topicsConfig = [
  { "topic" : "ViewStates" },
  { "topic" : "ViewActions" }
]

var kafkaManager = {}

kafkaManager.start = async () => {
  try {
    const kafka = new Kafka({
      "clientId" : "poc",
      "brokers" : ["172.17.0.3:9092"]
    });
    const admin = kafka.admin();
    await admin.connect();
    await iniciarTopicos(admin);
    await admin.disconnect();
  } catch(err) {
    console.error(`Erro ${err}`)
  }
}

async function iniciarTopicos(admin) {
  let kakfaTopics = await admin.listTopics();
  let topics = [];
  topicsConfig.forEach(topic => {
    let filtered = kakfaTopics.filter(kafkaTop => kafkaTop.topic === topic.topic);
    if (filtered.length === 0) {
      topics.push(topic);
    }
  })
  if (topics.length === 0) {
    console.log("Tópicos de ViewState e ViewActions já tinham sido criados no kafka");
    return;
  }
  let createdTopics = await admin.createTopics({ topics });
  console.log(`Tópicos criados no Kafka ${createdTopics}`);
}

export default kafkaManager;