import React from 'react';
import ReactDOM from 'react-dom';
import './kafka/StateManager';
import { BrowserView } from './root/BrowserView.js';
import { HomeView } from './root/home/HomeView.js';

HomeView.register();

ReactDOM.render(<BrowserView viewStateId={'0128fe121890'} />, document.getElementById('root'));